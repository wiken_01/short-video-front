import axios from "@/utils/axios"

export function recommendVideo() {
    return axios.get("/recommend/video")
}
