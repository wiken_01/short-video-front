import axios from "@/utils/axios"

export function addCollection(data) {
    return axios.post("/shortvideo/collection", data)
}

export function delCollection(data) {
    return axios.put("/shortvideo/collection", data)
}
