import axios from "@/utils/axios"

export function listComment(params) {
    return axios.get("/shortvideo/comment/comment_by_videoId", {params})
}

export function addComment(data) {
    return axios.post("/shortvideo/comment", data)
}

export function delComment(data) {
    return axios.put("/shortvideo/comment", data)
}