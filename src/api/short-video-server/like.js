import axios from "@/utils/axios"

export function addLike(data) {
    return axios.post("/shortvideo/like", data)
}

export function cancelLike(data) {
    return axios.put("/shortvideo/like", data)
}

