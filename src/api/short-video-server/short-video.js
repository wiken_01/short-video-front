import axios from "@/utils/axios"

export function listVideoByAuthorId(params) {
    return axios.get("/shortvideo/sv/video_by_authorid", {params})
}

export function listShortVideoByMineCollection(){
    return axios.get("/shortvideo/sv/by_mine_collection")
}

export function getShortVideoByMineLike(){
    return axios.get("/shortvideo/sv/by_mine_like")
}

export function getShortVideoByMinePublisher(){
    return axios.get("/shortvideo/sv/video_by_mine_publisher")
}

export function publisherVideo(data){
    return axios.post("/shortvideo/sv/publisher_video", data)
}
