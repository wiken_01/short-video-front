import axios from "@/utils/axios"

export function myinfo(){
    return axios.get("/tu/user/myinfo")
}

export function myFollow() {
    return axios.get("/tu/user/my_follow")
}

export function myFans(){
    return axios.get("/tu/user/my_fans")
}

export function follow(data){
    return axios.post("/tu/user/follow", data)
}

export function cancelFollow(data){
    return axios.put("/tu/user/cancel_follow", data)
}

export function login(params){
    return axios.get("/tu/login", {params})
}

export function register(data){
    return axios.post("/tu/register", data)
}