// import './assets/main.css'

import { createApp, provide } from 'vue'
import { createPinia } from 'pinia'

import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import 'element-plus/theme-chalk/dark/css-vars.css'

import * as ElementPlusIconsVue from '@element-plus/icons-vue'

import App from './App.vue'
import router from './router'
import axios from '@/utils/axios'

import 'xgplayer/dist/xgplayer.css'
const app = createApp(App)

app.use(createPinia())
app.use(router)

app.use(ElementPlus)

app.provide('axios', axios)
app.provide('imageBaseUrl', "http://localhost/static")
app.provide('uploadUrl', "http://localhost:8080/file/upload")

for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
    app.component(key, component)
  }

app.mount('#app')

