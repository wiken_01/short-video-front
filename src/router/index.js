import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import Recommend from "@/views/Recommend.vue"

import LikeVideoView from "@/views/LikeVideoView/index.vue"
import CollectionVideoView from "@/views/CollectionVideoView/index.vue"
import MineVideoView from "@/views/MineVideoView/index.vue"

import PublisherVideo from "@/views/PublisherVideoView/index.vue"

import LoginView from "@/views/LoginView/index.vue"
import RegisterView from "@/views/RegisterView/index.vue"

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
    {
      path: '/login',
      name: 'login',
      component: LoginView
    },
    {
      path: '/register',
      name: 'register',
      component: RegisterView
    },
    {
      path: '/recommend',
      name: 'recommend',
      component: Recommend,
    },
    {
      path: '/likeVideo',
      name: 'likeVideo',
      component: LikeVideoView
    },
    {
      path: '/collectionVideo',
      name: 'collectionVideo',
      component: CollectionVideoView
    },
    {
      path: '/mineVideo',
      name: 'mineVideo',
      component: MineVideoView
    },
    {
      path: '/publisher_video',
      name: 'publisherVideo',
      component: PublisherVideo,
    },
    {
      path: '/mine',
      name: 'mine',
      component: () => import("@/views/Mine.vue")
    }
  ]
})

export default router
