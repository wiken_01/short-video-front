import { ref, computed } from 'vue'
import { defineStore } from 'pinia'

export const useUserStore = defineStore('user', {
    state: () => ({ avatar: '',
                    username: '' }),
    getters: {
        getAvatar: (state) => state.avatar,
        getUsername: (state) => state.username,
        isLogin: (state) => state.avatar != '' && state.username != ''
    },
    actions: {
        setUser(user){
            
            this.avatar = user.avatar
            this.userame = user.username
        },
    },
})