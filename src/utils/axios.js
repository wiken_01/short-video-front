import axios from 'axios';
import router from '@/router';
axios.defaults.baseURL = 'http://localhost:8080/';

axios.interceptors.request.use(function (config) {
  let token = sessionStorage.getItem('token')

  if(token != null )
    config['headers']['Authorization'] = token
  // Do something before request is sent
  return config;
}, function (error) {
  // Do something with request error
  return Promise.reject(error);
});

axios.interceptors.response.use(function (response) {
    // Any status code that lie within the range of 2xx cause this function to trigger
    // Do something with response data
    let data = response.data;
  
    if(data.code == 401){
      router.push({name: 'login'})
    }

    return response.data
  }, function (error) {
    // Any status codes that falls outside the range of 2xx cause this function to trigger
    // Do something with response error
    return Promise.reject(error);
  });
export default axios