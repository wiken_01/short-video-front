import { theVideo, baseDomain, authorVideoList } from './domain.js'
import { listVideoByAuthorId } from '@/api/short-video-server/short-video'
import { follow } from '@/api/user-service/user'
import { hideSiderBar, hideSiderbarStatus } from './siderbar.js'

export function clickAuthorAvatarEvent() {
    listVideoOfAuthor(baseDomain.authorId)

    hideSiderBar(true, 'avatar')
}

export function clickFollowButtonEvent() {
    let followId = theVideo.authorId
    let data = {
        followId
    }

    follow(data)
        .then(res => {
            if (res.code == 200) {
                ElMessage({
                    message: res.msg,
                    type: 'success'
                })
            }

            else {
                ElMessage({
                    message: res.msg,
                    type: 'error'
                })
            }
        })
}


export function listVideoOfAuthor(authorId) {
    let query = {
        authorId
    }

    listVideoByAuthorId(query)
        .then(res => {
            if (res.code == 200) {
                authorVideoList.value = res.data
            }
        })
}
