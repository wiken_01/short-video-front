import { theVideo, baseDomain } from './domain.js'
import { addCollection, delCollection } from '@/api/short-video-server/collection'

export function clickCollectionEvent() {
    let videoId = theVideo.videoId
    let videoList = baseDomain.videoList
    let data = {
        videoId
    }

    if (!theVideo.collected) {
        addCollection(data)
            .then(res => {
                if (res.code == 200) {
                    theVideo.collected = true
                    videoList[baseDomain.videoIndex].collected = theVideo.collected

                    theVideo.collectionCount += 1
                    videoList[baseDomain.videoIndex].collection = theVideo.collectionCount
                }

                else {
                    ElMessage.error(res.msg)
                }
            })
    }

    else {
        delCollection(data)
            .then(res => {
                if (res.code == 200) {
                    theVideo.collected = false
                    videoList[baseDomain.videoIndex].collected = theVideo.collected

                    theVideo.collectionCount -= 1
                    videoList[baseDomain.videoIndex].collection = theVideo.collectionCount
                }

                else {
                    ElMessage.error(res.msg)
                }
            })
    }
}