import { onMounted, ref, reactive, inject, watch } from 'vue'

import { addComment, listComment } from '@/api/short-video-server/comment'
import { hideSiderBar, hideSiderbarStatus } from './siderbar.js'
import { theVideo, baseDomain } from './domain.js'

export const videoCommentList = reactive({
    value: []
})

export const commentContent = ref('')

export const commentPlaceHolder = ref('请输入评论')


export function getCommentList() {
    let videoId = theVideo.videoId

    let paramas = {
        videoId
    }
    listComment(paramas)
        .then(res => {
            if (res.code == 200) {
                videoCommentList.value = res.data
            }

            else {
                ElMessage.error(res.msg)
            }

        })
}

export function clickCommentEvent() {
    hideSiderBar(true, 'comment')
    getCommentList()
}
export function sendCommentEvent() {
    let comment = commentContent.value
    let videoId = theVideo.videoId
    let parentId = baseDomain.parentId
    let data = {
        comment,
        videoId,
        parentId,
    }

    addComment(data)
        .then(res => {
            if (res.code == 200) {
                commentContent.value = ''

                ElMessage({
                    message: res.msg,
                    type: 'success'
                })

                getCommentList()

                theVideo.commentCount += 1
                baseDomain.videoList[baseDomain.videoIndex].commentCount = theVideo.commentCount

                baseDomain.parentId = null
            }

            else {
                ElMessage.error(res.msg)
            }
        })
}

export function clickReplayEvent(commentVideoId, user) {
    baseDomain.parentId = commentVideoId

    let parentId = baseDomain.parentId
    let videoId = theVideo.videoId
    commentPlaceHolder.value = `回复 @${user.username}:`
    let data = {
        parentId,
        videoId,
    }
}


// export default{
//     clickCommentEvent,
//     clickReplayEvent,
//     sendCommentEvent,
//     getCommentList,
//     commentContent,
//     commentPlaceHolder,
//     videoCommentList,
// }