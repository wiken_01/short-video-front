import { onMounted, ref, reactive, inject, watch } from 'vue'


export const theVideo = reactive({
    videoId: null,
    likeCount: 0,
    commentCount: 0,
    collectionCount: 0,
    authorId: 0,
    liked: false,
    collected: false,
})

export let baseDomain = {
    authorId: null,
    videoList: [],
    videoIndex: null,
    parentId: null,
}


export const authorVideoList = reactive({
    value: []
})
