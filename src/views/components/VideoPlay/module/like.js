import { theVideo, baseDomain } from './domain.js'

import { addLike, cancelLike } from '@/api/short-video-server/like'

export function clickLikeEvent() {
    let videoId = theVideo.videoId
    let videoList = baseDomain.videoList
    let data = {
        videoId
    }

    if (!theVideo.liked) {
        addLike(data)
            .then(res => {
                if (res.code == 200) {
                    theVideo.liked = true
                    videoList[baseDomain.videoIndex].liked = true
                    theVideo.likeCount += 1
                    videoList[baseDomain.videoIndex].like = theVideo.likeCount
                }

                else {
                    ElMessage.error(res.msg)
                }
            })
    }

    else {
        cancelLike(data)
            .then(res => {
                if (res.code == 200) {
                    theVideo.liked = false
                    videoList[baseDomain.videoIndex].liked = false

                    theVideo.likeCount -= 1
                    videoList[baseDomain.videoIndex].like = theVideo.likeCount
                }
                else {
                    ElMessage.error(res.msg)
                }
            })
    }

}