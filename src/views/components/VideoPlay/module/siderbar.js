import { ref} from 'vue'


export function hideSiderBar(status, controller) {
    hideSiderbarStatus.value =
        status == undefined ? !hideSiderbarStatus.value : status

    siderbarController.value = controller
}

export const hideSiderbarStatus = ref(false);

export const siderbarController = ref("")

